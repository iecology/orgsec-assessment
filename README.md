This repository contains a fork of the [Security Readiness Assessment](https://0xacab.org/iecology/security-checklists/-/blob/master/2_readiness_assessment_tool.md) from our [Security Checklists set](https://0xacab.org/iecology/security-checklists) designed for standalone use and updated for the COVID-19 pandemic that began in 2020.

PLEASE NOTE THAT THESE DOCUMENTS ARE CURRENTLY IN FLUX DUE TO THE EMERGING SITUATION AND SITUATIONS MAY HAVE CHANGED SINCE LAST UPDATED. PAY ATTENTION TO THE LAST UPDATED DATE ON THIS ASSESSMENT TOOL TO BE SURE IT CAN MEET YOUR NEEDS.
